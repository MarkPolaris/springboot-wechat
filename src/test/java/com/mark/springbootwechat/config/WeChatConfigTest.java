package com.mark.springbootwechat.config;

import com.mark.springbootwechat.SpringbootWechatApplicationTests;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * @Author: MARK
 * @Date: 2019/9/10 10:47
 * @Version: 1.0.0
 * @Description:
 */
@Slf4j
public class WeChatConfigTest extends SpringbootWechatApplicationTests {
    @Resource
    private WeChatConfig weChatConfig;

    @Test
    public void getAppId() {
        log.info(weChatConfig.getAppId());
    }
}