package com.mark.springbootwechat.controller;

import com.mark.springbootwechat.config.WeChatConfig;
import com.mark.springbootwechat.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/9/10 11:00
 * @Version: 1.0.0
 * @Description:
 */
@RestController
@Slf4j
@RequestMapping(value = "/api/v1/test", produces = {"application/json;charset=UTF-8"})
@CrossOrigin
public class TestController {
    @Autowired
    private WeChatConfig weChatConfig;

    //测试配置读取
    @GetMapping("test_config")
    public String testConfig(){
        log.info(weChatConfig.getAppId());
        log.info(weChatConfig.getAppsecret());
        log.info("提交指定文件测试");
        log.info("提交指定文件在测试一次");
        return weChatConfig.getAppId();
    }

    @GetMapping("test_wechat")
    public List<User> testWechatApp(){
        List<User> users = new ArrayList<>();
        User user = new User();
        user.setName("用户1");
        User user1 = new User();
        user1.setName("用户2");
        users.add(user);
        users.add(user1);
        return users;
    }
}
