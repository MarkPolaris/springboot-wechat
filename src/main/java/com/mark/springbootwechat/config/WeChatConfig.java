package com.mark.springbootwechat.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: MARK
 * @Date: 2019/9/10 10:45
 * @Version: 1.0.0
 * @Description:
 */
@Configuration
// @PropertySource(value = "classpath:application.yml")
@Data
public class WeChatConfig {
    /**
     * @return
     * @create: 2019/9/10
     * @author MARK
     * @Description: 公众号appid
     */
    @Value("${wxpay.appid}")
    private String appId;
    /**
     * @return
     * @create: 2019/9/10
     * @author MARK
     * @Description: 公众号秘钥
     */
    @Value("${wxpay.appsecret}")
    private String appsecret;
}
