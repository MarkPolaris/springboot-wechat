package com.mark.springbootwechat.config;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author: MARK
 * @Date: 2019/10/13 14:30
 * @Version: 1.0.0
 * @Description: 包装返回结果，状态码，数据
 */
@Data
@AllArgsConstructor
public class Result {
    private Integer code;
    private Object data;
}
