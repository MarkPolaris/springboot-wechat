package com.mark.springbootwechat.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author: MARK
 * @Date: 2019/10/13 14:28
 * @Version: 1.0.0
 * @Description: 导航栏对象
 */
@Data
@AllArgsConstructor
public class NavItem {
    private String text;
    private Integer id;
}
