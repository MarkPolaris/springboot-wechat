package com.mark.springbootwechat.entity;

import lombok.Data;

import javax.persistence.Id;

@Data
public class Video {

  @Id
  private Integer id;
  private String title;
  private String summary;
  private String coverImg;
  private Integer viewNum;
  private Integer price;
  private java.sql.Timestamp createTime;
  private String online;
  private double point;

}
