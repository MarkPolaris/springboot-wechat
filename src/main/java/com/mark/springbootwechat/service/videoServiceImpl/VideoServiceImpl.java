package com.mark.springbootwechat.service.videoServiceImpl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mark.springbootwechat.entity.Video;
import com.mark.springbootwechat.mapper.VideoMapper;
import com.mark.springbootwechat.service.VideoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: MARK
 * @Date: 2019/9/10 10:32
 * @Version: 1.0.0
 * @Description:
 */
@Service
@Slf4j
public class VideoServiceImpl implements VideoService {
    @Resource
    private VideoMapper videoMapper;

    @Override
    public Map<String, Object> findAll(int page, int size) {
        log.info("当前页: " + page + " ," + size);
        PageHelper.startPage(page, size);
        List<Video> list = videoMapper.selectAll();
        PageInfo pageInfo = new PageInfo(list);

        Map<String, Object> data = new HashMap<>();
        //总条数
        data.put("total_size", pageInfo.getTotal());
        //总页数
        data.put("total_page", pageInfo.getPages());
        //当前页
        data.put("current_page", page);
        //数据
        data.put("data", pageInfo.getList());
        return data;
    }

    @Override
    public Video findById(Integer id) {
        return videoMapper.selectByPrimaryKey(id);
    }

    @Override
    public void update(Video video) {
        videoMapper.updateByPrimaryKeySelective(video);
    }

    @Override
    public void delete(Integer id) {
        videoMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void save(Video video) {
        videoMapper.insertSelective(video);
    }
}
