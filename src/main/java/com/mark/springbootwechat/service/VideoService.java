package com.mark.springbootwechat.service;

import com.github.pagehelper.PageInfo;
import com.mark.springbootwechat.entity.Video;

import java.util.List;
import java.util.Map;

/**
 * @Author: MARK
 * @Date: 2019/9/10 10:32
 * @Version: 1.0.0
 * @Description:
 */
public interface VideoService {
    Map<String, Object> findAll(int page, int size);

    Video findById(Integer id);

    void update(Video video);

    void delete(Integer id);

    void save(Video video);
}
