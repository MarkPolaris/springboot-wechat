package com.mark.springbootwechat.mapper;

import com.mark.springbootwechat.entity.Video;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: MARK
 * @Date: 2019/9/10 10:23
 * @Version: 1.0.0
 * @Description:
 */
@Mapper
public interface VideoMapper extends tk.mybatis.mapper.common.Mapper<Video> {
}
